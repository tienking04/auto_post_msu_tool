import win32com.client
from pandas import read_excel
from os import listdir, remove, mkdir
from os.path import isfile, join, exists, abspath

# covert color for excel
def rgbToInt(rgb):
    colorInt = rgb[0] + (rgb[1] * 256) + (rgb[2] * 256 * 256)
    return colorInt

# get dictionary of report spec names
def get_respec_names(xl, client_group):
    respec_name_file = "macro/All_report_spec.xlsx"
    respec_name_path = abspath(respec_name_file)
    respec_wb = xl.Workbooks.Open(respec_name_path)
    respec_ws = respec_wb.Sheets("MSU")
    respec_names = respec_ws.UsedRange.Value
    db_name_dict = {}
    for row in respec_names:
        db_name_dict[row[0]] = row[1]
    return db_name_dict

# add report spec name for each categories
def add_db_name(xl, client_group):
    db_name_dict = get_respec_names(xl, client_group)
    client_group["RE_SPEC NAME"] = client_group.apply (lambda row: db_name_dict[row['CATE_NAME']], axis=1)
    return client_group

# get configure information from conf.txt
def get_conf():
    conf_dict = {}
    with open("conf.txt", "r") as conf_file:
        cfc = conf_file.read().split("\n")
        while "" in cfc:
            cfc.remove("")
        for index in range(0,len(cfc),2):
            conf_dict[cfc[index]] = cfc[index+1]
    return conf_dict

# read client groups file (cate name, period)
def read_client_groups(client_group_name, client_group_sheet):
    full_clientgroup_name = abspath(client_group_name)
    client_group = read_excel(full_clientgroup_name,
                             sheet_name=client_group_sheet,
                             header=0,
                             usecols=[0, 1])
    return client_group

# get inspec file in each cate  
def get_inspec_path(client_group_row, prod_path):
    inspec_folder_path = join(prod_path, client_group_row["CATE_NAME"], "FinalInspection")
    if exists(inspec_folder_path):
        for f_name in listdir(inspec_folder_path):
            if isfile(join(inspec_folder_path, f_name)):
                f_name_list = f_name.split("_")
                if len(f_name_list) == 4 and f_name_list[0] == "Inspection" and f_name_list[2] == client_group_row["RE_SPEC NAME"] and f_name_list[3].split(".")[0] == str(client_group_row["PERIOD"]):
                    return (client_group_row["CATE_NAME"], f_name, join(inspec_folder_path, f_name))
        return (client_group_row["CATE_NAME"], client_group_row["CATE_NAME"] + "_" + client_group_row["RE_SPEC NAME"] + "_" + str(client_group_row["PERIOD"]), None)
    else:
        return (client_group_row["CATE_NAME"], None, None)

# read inspec file and get failed, skipped steps in each cate                   
def read_inspec_file(work_book, inspec_path):
    work_sheet = work_book.Sheets("Result Summary")
    result_summary = work_sheet.UsedRange.Value
    failed_step = []
    skipped_step = []
    for row in result_summary[1:]:
        if row[1] == "Failed":
            failed_step.append(row[0])
        if row[1] == "Skipped":
            skipped_step.append(row[0])
    return (failed_step, skipped_step)

# run copy sheet macro from macro file
def copy_sheet(xl, source_excel_path, sheet_name, result_excel_path, result_sheet_name):
    xl.Application.Run("copy_excel.xlsm!Copy.CopySheet", source_excel_path, sheet_name, result_excel_path, result_sheet_name)

    #xl.Application.Run("PERSONAL.XLSB!Module1.xlstoxlsmFinal", Datev)

# write summary sheet in result excel file
def write_summary(xl, result_path, failed_steps_sum):
    result_book = xl.Workbooks.Open(result_path)
    if "Summary" not in result_book.Sheets:
        summary_sheet = result_book.Sheets.Add(Before = result_book.Sheets(1))
        summary_sheet.Name = "Summary"
        summary_header = ("CATE NAME", "DATABASE NAME", "PERIOD", "FAILED STEP", "LINK", "NOTE")
        for index, head_cell in enumerate(summary_header):
            #(234,214,28)
            summary_sheet.Cells(1, index + 1).Value = head_cell
            summary_sheet.Cells(1, index + 1).Interior.Color = rgbToInt((166,158,176))
    else:
        summary_sheet = result_book.Sheets("Summary")
        
    for i, row in enumerate(failed_steps_sum):
            for j, cell in enumerate(row):
                if cell != None:
                    if summary_sheet.Cells(1, j + 2).Value == "NOTE":
                        if cell == "Skipped":
                            summary_sheet.Cells(i + 2, j + 2).Interior.Color = rgbToInt((238,143,27))
                            summary_sheet.Cells(i + 2, j + 2).Value = "Skipped"
                            continue

                    if summary_sheet.Cells(1, j + 1).Value == "FAILED STEP":
                        if cell == "PASS":
                            summary_sheet.Cells(i + 2, j + 1).Interior.Color = rgbToInt((0,255,0))
                            summary_sheet.Cells(i + 2, j + 1).Value = "PASS"
                        elif cell == "INF":
                            summary_sheet.Cells(i + 2, j + 1).Interior.Color = rgbToInt((234,214,28))
                            summary_sheet.Cells(i + 2, j + 1).Value = "Inspec file not found"
                        elif cell == "FNF":
                            summary_sheet.Cells(i + 2, j + 1).Interior.Color = rgbToInt((0,115,229))
                            summary_sheet.Cells(i + 2, j + 1).Value = "Folder not found"
                        else:
                            summary_sheet.Cells(i + 2, j + 1).Interior.Color = rgbToInt((255,0,0))
                            summary_sheet.Cells(i + 2, j + 1).Value = cell
                    else:
                        summary_sheet.Cells(i + 2, j + 1).Value = cell

    summary_sheet.Columns.AutoFit()
    #summary_sheet.UsedRange.ListObjects.Add().TableStyle = "TableStyleMedium15"
    result_book.Save()
    result_book.Close(SaveChanges=True)            

# copy failed sheet to result sheet and write summary
def copy_failed_sheet(xl, client_group, result_path, prod_path, client_group_sheet):
    if result_path[-1] != '\\':
        result_path += "\\"
    result_excel_path = join(result_path, client_group_sheet + ".xlsx")
    if len(client_group) > 0:
        name_check_dict = {"Check 5": ["Check5_S5A", "Check5_S5B"],
                          "Check 6": ["Check 6"],
                          "Check 7": ["Check7_S7A", "Check7_S7B"],
                          "Check 8": ["Check8_S8"],
                          "Check 9": ["Check9_S9"],
                          "Check 10": ["Check10"],
                          "Check 11": ["Check11"],
                          "Check 12": ["Check12_S12A", "Check12_S12B"],
                          "Check 14": ["Check14"],
                          "Check 15": ["Check15"],
                          "Check 16": ["Check16_S16"]}
        
        failed_steps_sum = []
        
        for _, row in client_group.iterrows():
            print("\n\t" + row["CATE_NAME"] + "\t" + row["RE_SPEC NAME"] + ":")
            cate_name, f_name, inspec_path = get_inspec_path(row, prod_path)
            if inspec_path == None and f_name != None:
                print("\t\t\tFI file doesn't exist")
                tmp_row = list(row)
                tmp_row.append("INF")
                tmp_row.append(None)
                failed_steps_sum.append(tmp_row)
                continue
            elif f_name == None:
                print("\t\t\tFolder doesn't exist")
                tmp_row = list(row)
                tmp_row.append("FNF")
                tmp_row.append(None)
                failed_steps_sum.append(tmp_row)
                continue
            work_book = xl.Workbooks.Open(inspec_path)
            hyperlink_form = r'=HYPERLINK(CELL("address",{0}!$A$1),"{1}")'
            
            failed_step, skipped_step = read_inspec_file(work_book, inspec_path)
            total_check_7 = post_step_7(cate_name, work_book, inspec_path, result_excel_path, f_name)
            if total_check_7 != {}:
                for step_7_name, sh_name in total_check_7.items():
                    tmp_row = list(row)
                    tmp_row.append(step_7_name)
                    tmp_row.append(hyperlink_form.format(sh_name, "LINK"))
                    failed_steps_sum.append(tmp_row)
            if failed_step != []:
                flag=False
                report_name = f_name.split(".")[0].split("_")[2]
                #ActiveCell.FormulaR1C1 = "=HYPERLINK(""[Workbook.xlsx]Sheet1!A1"",""CLICK HERE"")"
                
                for step in failed_step:
                    print("\t\t\t" + step)
                    step_number = step.split(" - ")[0]
                    sh_name = cate_name + "_" + report_name + "_"
                    if flag==False and step_number in ("Check 1", "Check 2", "Check 3", "Check 4", "Check 13"):
                        copy_sheet(xl, inspec_path, "Check1To4,13", result_excel_path, sh_name + "Check1To4.13")
                        flag = True
                    if step_number in ("Check 1", "Check 2", "Check 3", "Check 4", "Check 13"):
                        tmp_row = list(row)
                        tmp_row.append("Check1To4,13")
                        tmp_row.append(hyperlink_form.format(sh_name + "Check1To4.13", "LINK"))
                        failed_steps_sum.append(tmp_row)
                    if step_number in name_check_dict.keys():
                        for inspec_name_step in name_check_dict[step_number]:
                            copy_sheet(xl, inspec_path, inspec_name_step, result_excel_path, sh_name + inspec_name_step)
                            tmp_row = list(row)
                            tmp_row.append(inspec_name_step)
                            tmp_row.append(hyperlink_form.format(sh_name + inspec_name_step, "LINK"))
                            failed_steps_sum.append(tmp_row)
            
            if skipped_step != []:
                report_name = f_name.split(".")[0].split("_")[2]
                for step in skipped_step:
                    print("\t\t\t" + "Skipped: " + step)
                    step_number = step.split(" - ")[0]
                    if int(step_number.split(" ")[1]) in range(1, 17):
                        tmp_row = list(row)
                        tmp_row.append(step_number)
                        tmp_row.append("Skipped")
                        failed_steps_sum.append(tmp_row)
                    else:
                        skipped_step.remove(step)

            if failed_step == [] and skipped_step == [] and total_check_7 == {}:
                tmp_row = list(row)
                tmp_row.append("PASS")
                tmp_row.append(None)
                failed_steps_sum.append(tmp_row)
            
            print("\t\t\tDone")
                
        write_summary(xl, result_excel_path, failed_steps_sum)

# post for step 7
def post_step_7(cate_name, work_book, inspec_path, result_excel_path, f_name):
        total_check = {}
        if "Check7_S7A" in work_book.Sheets:
            work_sheet = work_book.Sheets("Check7_S7A")
            result_summary = work_sheet.UsedRange.Value
            for row in result_summary[1:]:
                if int(row[6]) > 0.9 and int(row[6]) < -0.9:
                    report_name = f_name.split(".")[0].split("_")[2]
                    paste_sheet_name = cate_name + "_" + report_name + "_Check7_S7A"
                    copy_sheet(inspec_path, "Check7_S7A", result_excel_path, paste_sheet_name)
                    total_check["Check7_S7A"] = paste_sheet_name
        if "Check7_S7B" in work_book.Sheets:
            work_sheet = work_book.Sheets("Check7_S7B")
            result_summary = work_sheet.UsedRange.Value
            for row in result_summary[1:]:
                if int(row[6]) > 0.9 and int(row[6]) < -0.9:
                    report_name = f_name.split(".")[0].split("_")[2]
                    paste_sheet_name = cate_name + "_" + report_name + "_Check7_S7B"
                    copy_sheet(inspec_path, "Check7_S7B", result_excel_path, paste_sheet_name)
                    total_check["Check7_S7B"] = paste_sheet_name
        return total_check

# main function
def main():
    try:
        xl=win32com.client.Dispatch("Excel.Application")
        xl.Visible = False
        xl.DisplayAlerts = False
        conf_dict = get_conf()
        
        xl.Workbooks.Open(abspath("macro/copy_excel.xlsm"))
        client_group_sheet = input("Enter sheet name: ")
        
        if not exists("Result"):
            mkdir("Result")
        full_result_path = abspath("Result")
        result_excel_path = join(full_result_path, client_group_sheet + ".xlsx")
        if exists(result_excel_path):
            remove(result_excel_path)
        workbook = xl.Workbooks.Add()
        workbook.SaveAs(result_excel_path)
        
        client_group = read_client_groups(conf_dict['[CLIENT GROUPS NAME]'], client_group_sheet)
        client_group = add_db_name(xl, client_group)
        copy_failed_sheet(xl, client_group, full_result_path, conf_dict['[PROD PATH]'], client_group_sheet)
    finally:
        xl.Application.Quit()
        del xl
   
if __name__ == '__main__':
    main()